require "course"

class Student
  # Student#initialize should take a first and last name.
  # Student#name should return the concatenation of the student's first and last name.
  # Student#courses should return a list of the Courses in which the student is enrolled.
  # Student#enroll should take a Course object, add it to the student's list of courses, and update the Course's list of enrolled students.
  # enroll should ignore attempts to re-enroll a student.
  # Student#course_load should return a hash of departments to # of credits the student is taking in that department.

  def initialize(name, surname)
    @first_name = name
    @last_name = surname
    @courses = []
  end

  attr_reader :first_name, :last_name, :courses

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    if self.has_conflict?(course)
      raise "This course conflicts with another course in your schedule."
    end
    unless @courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def course_load
    load_hash = {}
    @courses.each do |el|
      dept = el.department
      creds = el.credits
      if load_hash.key?(dept)
        load_hash[dept] += creds
      else
        load_hash[dept] = creds
      end
    end
    load_hash
  end

  def has_conflict?(course_to_add)
    @courses.any? { |el| el.conflicts_with?(course_to_add)}
  end
end
